package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"sort"

	"trie"
)

const (
	usage = `usage: word-count  file_name`
	topN  = 20
)

func main() {

	if len(os.Args) < 2 || len(os.Args) > 2 {
		fmt.Printf("Invalid number of parameters\n %s\n", usage)
		return
	}

	fileName := os.Args[1]

	file, err := os.Open(fileName)

	if err != nil {
		log.Printf("failed to open file: %s\n", err)
		return
	}

	defer file.Close()

	// Create a scanner that will read the file word by word
	wordScanner := bufio.NewScanner(file)
	wordScanner.Split(bufio.ScanWords)

	wordStore := trie.New()

	for wordScanner.Scan() {
		wordStore.Add([]rune(wordScanner.Text()))
	}

	wordCounts := wordStore.Walk()

	// Sort the slice using the count value stored in each WordCount
	sort.Slice(wordCounts, func(i, j int) bool {
		return wordCounts[j].Count < wordCounts[i].Count
	})

	topCounts := make([]trie.WordCount, 0)

	if len(wordCounts) < topN {
		topCounts = wordCounts
	} else {
		topCounts = wordCounts[:topN]
	}

	for _, value := range topCounts {
		log.Printf("%d %v\n", value.Count, string(value.Word))
	}
}
