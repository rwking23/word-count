package trie

import (
	"unicode"
)

const (
	alphabetLenth = 26
	asciiaValue   = 97
)

// WordCount stores a word along with the number of times it's
// appeared in the trie
type WordCount struct {
	Word  []rune
	Count int
}

type node struct {
	letter   rune
	count    int
	terminus bool
	// a map could be used for the array of children but task
	// asks not to use maps.
	children [alphabetLenth]*node
}

// walk iterates over all the children in the node and checks
// for any complete words. When a word is found it is added to a
// list of complete words. If no word is found it recursivley calls walk
// on each non-nil child in turn until it reaches a terminus node
// where we know a word count value will exist. It then unwinds
// each branch in turn.
func (n *node) walk(runes []rune, wordCounts *[]WordCount) {

	// Unwind the branch if we have reached the end
	// saves searching an array of nil children
	if n.terminus {
		if n.count > 0 {
			// To store the current runes as a word we need
			// to copy it to a new slice as when we next modify
			// runes it will modify the underlying array
			word := make([]rune, len(runes))
			copy(word, runes)

			wc := WordCount{
				Word:  word,
				Count: n.count,
			}
			*wordCounts = append(*wordCounts, wc)
		}
		runes = runes[:len(runes)-1]
		return
	}

	for _, child := range n.children {

		if child == nil {
			continue
		}
		runes = append(runes, child.letter)

		// If the count of a node is greater than 0 then
		// the current runes in the array are a word
		if child.count > 0 {

			// To store the current runes as word we need
			// to copy it to a new slice as when we next modify
			// runes it will modify the underlying array
			word := make([]rune, len(runes))
			copy(word, runes)

			wc := WordCount{
				Word:  word,
				Count: child.count,
			}
			*wordCounts = append(*wordCounts, wc)
		}
		child.walk(runes, wordCounts)
		runes = runes[:len(runes)-1]
	}
	// Remove the current node rune from the slice so it
	// is not counted in the next word
	runes = runes[:len(runes)-1]
}

func (n *node) newChild(index int, letter rune) *node {

	newNode := &node{letter: letter}
	n.children[index] = newNode
	return newNode

}

// Trie structure for storing words along with a count for a
// particular word in the node corresponding to the end of the word
type Trie struct {
	root *node
}

// New creates a new Trie
func New() *Trie {
	return &Trie{
		root: &node{terminus: true},
	}
}

// Add adds the word to the trie, if the word already exists
// then the count for that word is incremented. All words are
// stored lower case.
func (t *Trie) Add(word []rune) {

	n := t.root

	for _, letter := range word {

		// Only looking for words so will ignore special
		// characters
		if !unicode.IsLetter(letter) {
			continue
		}

		lower := unicode.ToLower(letter)
		index := lower - asciiaValue
		// A character we can't handle
		if index < 0 || index > 25 {
			continue
		}

		if n.children[index] == nil {

			n.terminus = false
			n = n.newChild(int(index), lower)

		} else {
			n = n.children[index]
		}
	}
	n.count++
}

// Walk traverses the trie creating a list of all the present words
// along with the count of the number of times they have been
// added to the trie.
func (t *Trie) Walk() []WordCount {

	wordCounts := make([]WordCount, 0)

	for _, n := range t.root.children {

		// Maintain a slice of runes for each branch from the root
		// node down. When a complete word is found this can then
		// be turned into a string
		runes := make([]rune, 0)

		if n == nil {
			continue
		}
		runes = append(runes, n.letter)

		if n.count > 0 {
			word := make([]rune, len(runes))
			copy(word, runes)

			wc := WordCount{
				Word:  word,
				Count: n.count,
			}
			wordCounts = append(wordCounts, wc)
		}

		n.walk(runes, &wordCounts)
	}
	return wordCounts
}
