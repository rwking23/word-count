package trie

import (
	"testing"

	. "gopkg.in/check.v1"
)

func Test(t *testing.T) { TestingT(t) }

type TrieTestSuite struct{}

var _ = Suite(&TrieTestSuite{})

func (t *TrieTestSuite) TestNew(c *C) {

	testTrie := New()

	c.Assert(testTrie, NotNil)
	c.Check(len(testTrie.root.children), Equals, 26)
}

func (t *TrieTestSuite) TestAdd(c *C) {

	testTrie := New()
	word := []rune("hello")

	testTrie.Add(word)

	runes := []rune(word)
	node := testTrie.root
	for _, r := range runes {

		nextNode := node.children[int(r-asciiaValue)]
		c.Check(nextNode, NotNil)
		node = nextNode
	}

	c.Check(node.count, Equals, 1)
}

func (t *TrieTestSuite) TestAddNonLetters(c *C) {

	testTrie := New()
	word := []rune("***")

	testTrie.Add(word)

	// Check no characters have been added
	for _, child := range testTrie.root.children {
		c.Check(child, IsNil)
	}

}

func (t *TrieTestSuite) TestAddSameWord(c *C) {

	testTrie := New()
	word := []rune("hello")

	testTrie.Add(word)
	testTrie.Add(word)

	runes := []rune(word)
	node := testTrie.root
	for _, r := range runes {

		nextNode := node.children[int(r-asciiaValue)]
		c.Check(nextNode, NotNil)
		node = nextNode
	}

	c.Check(node.count, Equals, 2)
}

func (t *TrieTestSuite) TestWalk(c *C) {

	testTrie := New()
	c.Assert(testTrie, NotNil)
	word := []rune("hello")

	testTrie.Add(word)

	counts := testTrie.Walk()

	c.Check(len(counts), Equals, 1)
	c.Check(counts[0].Word, DeepEquals, word)
	c.Check(counts[0].Count, Equals, 1)
}

func (t *TrieTestSuite) TestWalkSplitBranch(c *C) {

	testTrie := New()
	c.Assert(testTrie, NotNil)
	word1 := []rune("this")
	word2 := []rune("the")

	testTrie.Add(word1)
	testTrie.Add(word2)

	counts := testTrie.Walk()

	c.Check(len(counts), Equals, 2)
	c.Check(counts[0].Word, DeepEquals, word2)
	c.Check(counts[0].Count, Equals, 1)
	c.Check(counts[1].Word, DeepEquals, word1)
	c.Check(counts[1].Count, Equals, 1)
}

func (t *TrieTestSuite) TestWalkMultipleWords(c *C) {

	testTrie := New()
	c.Assert(testTrie, NotNil)
	word1 := []rune("hello")
	word2 := []rune("worlds")
	word3 := []rune("world")

	testTrie.Add(word1)
	testTrie.Add(word2)
	testTrie.Add(word3)
	testTrie.Add(word3)

	counts := testTrie.Walk()

	c.Check(len(counts), Equals, 3)
	c.Check(counts[0].Word, DeepEquals, word1)
	c.Check(counts[1].Word, DeepEquals, word3)
	c.Check(counts[1].Count, Equals, 2)
	c.Check(counts[2].Word, DeepEquals, word2)
}
