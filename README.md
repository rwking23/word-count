# word-count

Counts words in the provided text file using a trie.

To run using docker first build the image using 
`docker build -t word-count .` then run using
`docker run word-count /test_files/mobydick.txt`

Other wise the program can be built locally by setting
the go path to the top of the checkout and running `cd src/bin && go build -o word-count` to create the binary. Then run it using `src/bin/word-count test_files/mobydick.txt`