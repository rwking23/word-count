FROM golang:1.11.2-alpine3.8

ADD . /

WORKDIR /src/bin

ENV GOPATH=/

RUN go get ./... && go build -o word-count

ENTRYPOINT ["/src/bin/word-count"]